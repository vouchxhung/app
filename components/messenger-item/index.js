import React from 'react'
import { useRouter } from 'next/router'
import classnames from 'classnames'
import styles from './index.module.css'

export default function MessengerItem({ message }) {
  const router = useRouter()
  const {u: user} = router.query
  if (message.username === user) {
    return (
      <div className='flex justify-end message-row' id={message._id} key={message._id}>
        <div className={classnames(styles.msg, styles.me)}>{message.content}</div>
      </div>
    )
  }
  return (
    <div key={message._id} id={message._id} className='message-row'>
      <div>{message.username}</div>
      <div className='w-full'>
        <div className={classnames(styles.msg, styles.client)}>{message.content}</div>
      </div>
    </div>
  )
}
