import styles from './index.module.css'
import ArrowIcon from '../../public/up.svg'
import { useRouter } from 'next/router'
import { useEffect, useRef, useState } from 'react'
import { listMsg } from '../../services'
import MessengerItem from '../messenger-item'
import useElementOnScreen from '../../hooks/useElementOnScreen'

const pageSize = 10

export default function Messenger({ socket }) {
  const router = useRouter()
  const {u: user, room: roomId} = router.query
  const [msg, setMsg] = useState('')
  const [page, setPage] = useState(0)
  const [messages, setMessages] = useState([])

  useEffect(() => {
    if (!roomId || !user) return
    socket.emit('joinRoom', {
      username: user,
      roomId
    })
    socket.on('connected', ({ error, roomId }) => {
      if (error) return handleExit()
      listMsg({ roomId, pageSize, offset: 0 }).then(res => setMessages(res.data))
    })
  }, [roomId])

  useEffect(() => {
    socket.on("message", (res) => {
      setMessages((prevMessages) => {
        const _messages = [...prevMessages]
        _messages.push(res)
        return _messages
      })
    });
  }, [socket])

  const handleExit = () => {
    window.location.href='/'
  }
  const handleSubmit = async () => {
    if (!msg) return
    socket.emit('chat', {
      username: user,
      roomId,
      message: msg
    })
    setMsg('')
  }

  /**
   * Scroll to bottom on receiving new message
   */
  const messagesEndRef = useRef(null)
  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" })
  }
  useEffect(scrollToBottom, [messages])

  /**
   * Handle submit message when user trike Enter
   */
  useEffect(() => {
    const listener = event => {
      if (event.code === "Enter" || event.code === "NumpadEnter") {
        event.preventDefault()
        handleSubmit()
      }
    }
    document.addEventListener("keydown", listener)
    return () => {
      document.removeEventListener("keydown", listener)
    }
  }, [msg])

  /**
   * Handle load more message when user scroll to the top of screen
   */
  const [ isVisible ] = useElementOnScreen({
    root: null,
    rootMargin: "0px",
    threshold: 1.0,
    el: messages[0]?._id
  })
  useEffect(() => {
    if (!isVisible) return
    listMsg({
      roomId,
      pageSize,
      offset: (page+1)*pageSize
    }).then(res => {
      if (res.data.length === 0) return
      setMessages((prevMessages) => {
        return [...res.data, ...prevMessages]
      })
      setPage(page+1)
    })
  }, [isVisible])

  return (
    <>
      <div className='flex flex-col justify-center pt-10'>
        <div className='grid grid-cols-5 mb-4'>
          <div className={styles.exitBtn} onClick={() => handleExit()}>Exit</div>
          <h1 className='col-span-1 text-center'>
            {roomId}
          </h1>
        </div>
      </div>
      <div className={styles.conversation}>
        <div className={styles.container} id='conversation'>
          {messages
          .sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime())
          .map(message => <MessengerItem key={message._id} message={message} />)}
          <div ref={messagesEndRef} />
        </div>
      </div>

      <div className={styles.send}>
        <input
          value={msg}
          placeholder='Message here...'
          onChange={e => setMsg(e.target.value)}
        />
        <div className={styles.sendBtn} onClick={() => handleSubmit()}>
          <ArrowIcon
            alt="Arrow icon"
            fill='#fff'
            className='h-6 w-6 text-white'
          />
        </div>
      </div>
    </>
  )
}
