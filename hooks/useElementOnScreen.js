import { useEffect, useState } from 'react'

export default function useElementOnScreen(options) {
  if (typeof window === 'undefined') return []
  const [ active, setActive ] = useState(0)
  const [ isVisible, setIsVisible ] = useState(false)

  const callbackFunction = (entries) => {
    const [ entry ] = entries
    if (entry.isIntersecting) setActive(active+1)
    setIsVisible(active>3 && entry.isIntersecting)
  }

  useEffect(() => {
    const observer = new IntersectionObserver(callbackFunction, options)
    const el = document.getElementById(options.el)
    if (el) observer.observe(el)

    return () => {
      if(el) observer.unobserve(el)
    }
  }, [options])

  return [isVisible]
}

