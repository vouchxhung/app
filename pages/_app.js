import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  return (
    <div className='flex justify-center'>
      <div className='max-w-sm w-full h-screen bg-white'>
        <Component {...pageProps} />
      </div>
    </div>
  )
}

export default MyApp
