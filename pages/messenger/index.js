import Messenger from '../../components/messenger'
import io from 'socket.io-client'
import { baseURL } from '../../services'

export default function MessengerPage(){
  const socket = io.connect(baseURL)

  return (
    <div className='px-2 h-screen'>
      <Messenger socket={socket} />
    </div>
  )
}
