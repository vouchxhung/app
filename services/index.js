import axios from "axios";

export const baseURL = process.env.NEXT_PUBLIC_API_URL

const instance = axios.create({
  baseURL,
  timeout: 1000,
})

export const listMsg = (params) => {
  return instance.get("/message", { params }).then((res) => res.data)
}

export const sendMsg = (params) => {
  return instance.post("/message", params).then((res) => res.data)
}
